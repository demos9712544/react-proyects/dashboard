
//components
export { PokemonsGrid } from './components/PokemonsGrid' 

//interface
export type { SimplePokemon  } from './interfaces/simple-pokemon.interface'
export type {  PokemonsResponse } from './interfaces/pokemons-response'
export type { Pokemon } from './interfaces/pokemon'