import { CartCounter } from "@/shopping-cart"

   
  
  
  export const metadata = {
      title: 'Counter page',
      description: 'Counter page',
    }
    
    export default function CounterPage() {
        

    return (
        <>
           <div className="flex flex-col items-center justify-center w-full h-full">
            <span>Product in the chart</span>
            <CartCounter value= {20}/>
           </div>
        </>
    )
}