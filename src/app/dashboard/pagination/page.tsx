import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

export default function PaginationPage() {
  return (
    <div className='flex flex-col items-center justify-around justify-evenly h-full'>
         <Stack spacing={2}>
            <Pagination count={23} />
        </Stack>
        <Stack>
            <p className='font-bold'>Primary</p>
            <Pagination count={5} color="primary" />
        </Stack>
        <Stack>
            <p className='font-bold'>Secondary</p>
            <Pagination count={14} color="secondary" />
        </Stack>
        <Stack>
            <p className='font-bold'>Disabled</p>
            <Pagination count={55} disabled />
        </Stack>
            
      
    </div>
  )
}
