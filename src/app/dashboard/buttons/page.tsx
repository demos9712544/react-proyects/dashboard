import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';
import DeleteIcon from '@mui/icons-material/Delete';
import Stack from '@mui/material/Stack';
import IconButton from '@mui/material/IconButton';
import Fingerprint from '@mui/icons-material/Fingerprint'

  export const metadata = {
      title: 'Buttons page',
      description: 'Buttons page',
    }
    
    export default function ButtonsPage() {
        

    return (
        <>
           <div className="flex items-center justify-around w-full h-full">
            <div className='me-5'>
              <Button className=' text-black me-5 hover:bg-gray-400' variant="text">Text</Button>
              <Button className='bg-blue-500 me-5 text-white hover:bg-gray-400 my-20' variant="contained">Contained</Button>
              <Button className='me-5 hover:bg-gray-400' variant="outlined">Outlined</Button>
              <Button className='my-20 me-5' variant="contained" disabled>Disabled</Button>
            </div>
            <div className='me-5 flex flex-col'>
                <p className='mb-3 font-bold'>Outlined Button with icon</p>
                <Stack direction="row" spacing={4}>
                    <Button variant="outlined" startIcon={<DeleteIcon />}>
                      Delete
                    </Button>
                    <Button variant="outlined" endIcon={<SendIcon />}>
                      Send
                    </Button>
                  </Stack>
                  <p className='mt-6 font-bold'>Fingerprint button</p>
                  <Stack direction="row" spacing={2}>
                    <IconButton aria-label="fingerprint" color="secondary">
                      <Fingerprint />
                    </IconButton>
                    <IconButton aria-label="fingerprint" color="success">
                      <Fingerprint />
                    </IconButton>
                  </Stack>

                  <p className='mt-6 font-bold'>Sizes</p>
                  <Stack direction="row" alignItems="center" spacing={1}>
                    <IconButton aria-label="delete" size="small">
                      <DeleteIcon fontSize="inherit" />
                    </IconButton>
                    <IconButton aria-label="delete" size="small">
                      <DeleteIcon fontSize="small" />
                    </IconButton>
                    <IconButton aria-label="delete" size="large">
                      <DeleteIcon />
                    </IconButton>
                    <IconButton aria-label="delete" size="large">
                      <DeleteIcon fontSize="inherit" />
                    </IconButton>
                  </Stack>
              </div>
           </div>
        </>
    )
}