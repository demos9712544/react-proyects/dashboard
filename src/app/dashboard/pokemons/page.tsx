import { PokemonsResponse } from "@/pokemons"
import { PokemonsGrid } from "@/pokemons/components/PokemonsGrid"


const getPokemons = async( limit = 20, offset = 0 ) => {
    const data: PokemonsResponse = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
    .then(res => res.json())
   
    const pokemons = data.results.map( pokemon => ({
      id: pokemon.url.split('/').at(-2)!,
      name: pokemon.name,
      url: pokemon.url
    }))

    // throw new Error('Could not find pokemons');
   
    return pokemons
  }


export default async function PokemonsPage() {


        
    const pokemons = await getPokemons(151)


  return (
    <div className="flex flex-col">

      <span className="text-4xl my-2">Pokemon List <small>Static</small></span>

      <PokemonsGrid pokemons={pokemons}/>
        
    </div>
  )
}

