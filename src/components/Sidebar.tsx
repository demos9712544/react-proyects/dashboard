import Image from "next/image"
import { SidebarMenuItem } from "./SidebarMenuItem"
import { IoLogoReact ,IoBrowsersOutline, IoCalculator, IoInvertModeSharp, IoCodeSlashOutline } from "react-icons/io5";
import { MdCatchingPokemon } from "react-icons/md";




export const Sidebar = () => {

    const menuItems = [
        {
            path: '/dashboard/main',
            icon: <IoBrowsersOutline size={40}/>,
            title: 'Dashboard',
            subtitle: 'Visualization'
        },
        {
            path: '/dashboard/buttons',
            icon: <IoInvertModeSharp size={40}/>,
            title: 'Buttons',
            subtitle: 'UI Buttons'
        },
        {
            path: '/dashboard/pagination',
            icon: <IoCodeSlashOutline size={40}/>,
            title: 'Pagination',
            subtitle: 'Component for pagination'
        },
        {
            path: '/dashboard/counter',
            icon: <IoCalculator size={40}/>,
            title: 'Counter',
            subtitle: 'Counter Client Side'
        },
        {
            path: '/dashboard/pokemons',
            icon: <MdCatchingPokemon size={40}/>,
            title: 'Pokemons',
            subtitle: 'Pokemons API'
        }
    ] 

    return(

        <div 
            id="menu"
            style={ { width: '400px' } } 
            className="bg-gray-900 min-h-screen z-10 text-slate-300 w-64 left-0  overflow-y-scroll"
            >
            <div id="logo" className="my-4 px-6">
                <h1 className="text-lg md:text-2xl font-bold text-white">
                    <div className="flex align-items-center">
                        <IoLogoReact/>
                        <span>Library </span>
                    <span className="text-blue-500">React</span>
                    </div>
                    </h1>
                <p className="text-slate-500 text-sm mt-4">My personal site where you can see my experience, projects and much more.</p>
            </div>
            <div id="profile" className="px-6 py-10">
                <p className="text-black-500 mb-3">Welcome to my Portfolio </p>
                <a href="#" className="inline-flex space-x-2 items-center">
                    <span>
                    <Image
                        className="rounded-md"
                        src="/images/profile.jpeg"
                        width={50}
                        height={50}
                        alt="Picture of the author"
                        />
                    </span>
                    <span className="text-sm md:text-base font-bold">
                        Matias Michelli
                    </span>
                    </a>
            </div>
            <div id="nav" className="w-full px-6">

                {menuItems.map( item => (
                    <SidebarMenuItem
                        key={ item.path } 
                        {...item}/>

                ))}
                
            </div>
        </div>   
    )
}
   