This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

#Features

1. This project made a dashboard with Tailwind CSS.

2. I created a Sidebar component with different buttons to navigate dynamic routes by ID. 

3. I make http requests to the Pokemon API. 

4. Creation of dynamic metadata. 

5. Creation of not-found component in case the request to the API does not find the requested resources on the client side.

## Getting Started

1. Clone project.
2. npm i.
3. npm run dev.